#!/bin/bash

source /opt/fortify-controller/tomcat/webapps/scancentral-ctrl/WEB-INF/classes/config.properties

# export JAVA_OPTS="$JAVA_OPTS -Djavax.net.ssl.trustStore=$(readlink -f /usr/bin/java | sed "s:bin/java::")lib/security/cacerts" "-Djavax.net.ssl.trustStorePassword=${CA_CERTS_PASSWORD:-changeit}"
# export JAVA_TOOL_OPTIONS="$JAVA_TOOL_OPTIONS -Djavax.net.ssl.trustStore=$(readlink -f /usr/bin/java | sed "s:bin/java::")lib/security/cacerts" "-Djavax.net.ssl.trustStorePassword=${CA_CERTS_PASSWORD:-changeit}"

/opt/fortify-controller/tomcat/bin/catalina.sh run

