FROM ubuntu:20.04

ARG RESOURCE_PATH=./resources
ARG CREATED_USER=ciuser
ARG FORTIFY_CONTROLER_VERSION=22.2.0
ARG FORTIFY_CONTROLER_HOME=/opt/fortify-controller

# "${RESOURCE_PATH}/customcacerts",

# Get Packages / Binaries dependencies
RUN apt-get update && apt-get install wget -y

ARG PKGREGISTRY_JOB_TOKEN
ARG TOKEN=$PKGREGISTRY_JOB_TOKEN
ARG TOKEN_TYPE=JOB-TOKEN

RUN wget --header "${TOKEN_TYPE}:${TOKEN}" "https://gitlab.com/api/v4/projects/36628660/packages/generic/fortify-scancentral-controller/${FORTIFY_CONTROLER_VERSION}/Fortify_ScanCentral_Controller_${FORTIFY_CONTROLER_VERSION}_x64.zip" -P "/root/"

COPY [ "${RESOURCE_PATH}/docker-entrypoint.sh", "${RESOURCE_PATH}/config.properties", "/root/" ]

RUN export DEBIAN_FRONTEND=noninteractive                                                               && \
    adduser --disabled-password --shell /bin/bash --home /home/${CREATED_USER} ${CREATED_USER}          && \
    # Add Entrypoints
    mkdir -p /entrypoints                                                                               && \
    mv /root/*-entrypoint.sh /entrypoints                                                               && \
    chmod +x /entrypoints/*                                                                             && \
    chown ${CREATED_USER} -R /entrypoints                                                               && \
    # Install needed tools
    apt-get update                                                                                      && \
    apt-get install -y wget curl gnupg2 ca-certificates lsb-release apt-transport-https                 \
    unzip software-properties-common                                                                    && \
    # Add needed custom repositories
    wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | apt-key add -            && \
    add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/                            && \
    apt-get update                                                                                      && \
    # Install OpenJDK 11
    apt-get install -y adoptopenjdk-11-hotspot unzip                                                     && \
    # Install Fortify Controller
    unzip /root/Fortify_ScanCentral_Controller_${FORTIFY_CONTROLER_VERSION}_x64.zip -d ${FORTIFY_CONTROLER_HOME} && \
    rm /root/Fortify_ScanCentral_Controller_${FORTIFY_CONTROLER_VERSION}_x64.zip                        && \
    chmod +x ${FORTIFY_CONTROLER_HOME}/tomcat/bin/*.sh                                                  && \
    rm ${FORTIFY_CONTROLER_HOME}/tomcat/webapps/scancentral-ctrl/WEB-INF/classes/config.properties      && \
    cp /root/config.properties ${FORTIFY_CONTROLER_HOME}/tomcat/webapps/scancentral-ctrl/WEB-INF/classes/config.properties && \
# # Certif
# chown -R ${CREATED_USER} /root/customcacerts                                                        && \
# cp /root/customcacerts $(readlink -f /usr/bin/java | sed "s:bin/java::")lib/security/cacerts        && \
# chmod +r $(readlink -f /usr/bin/java | sed "s:bin/java::")lib/security/cacerts                      && \
# Give rights and clean
    chown ${CREATED_USER} -R /opt/*                                                                     && \
    rm -r /root/*                                                                                       

USER ${CREATED_USER}                                        

ENV CATALINA_HOME=${FORTIFY_CONTROLER_HOME}/tomcat                                                      \
    CATALINA_BASE=$CATALINA_HOME                                                                        \
    FORTIFY_CONTROLER_VERSION=${FORTIFY_CONTROLER_VERSION}                                              \
    FORTIFY_CONTROLER_HOME=${FORTIFY_CONTROLER_HOME}                                                    \
    USER_HOME=/home/${CREATED_USER}                                                                     \
    CREATED_USER=${CREATED_USER}                                                                        \
    PATH=${CATALINA_HOME}/bin:${PATH}                                                                   


ENTRYPOINT [ "/entrypoints/docker-entrypoint.sh" ]
